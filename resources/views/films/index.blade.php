@extends('adminLTE.master')

@section('content')

            <div class="card-header">
                <h3 class="card-title">Bordered Table</h3>
            </div>

            <div class="card-body">
            @if(session ('success'))
            <div class="alert alert-success">
              {{ session('success')}}
            </div>
          @endif

                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Title Film</th>
                      <th>Sinopsis</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  @forelse($films as $key => $film)
                        <tr>
                          <td> {{ $key + 1 }}</td>
                          <td> {{ $film->title }} </td>
                          <td> {{ $film->body }} </td>
                          <td style="display : flex;"> 
                            <a href="/films/{{$film->id}}" class="btn btn-info">Show</a>
                            <a href="/films/{{$film->id}}/edit" class="ml-3 btn btn-default">Edit</a>

                            <form action="/films/{{$film->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="ml-3 btn btn-danger">
                            </form>
                          </td>
                        </tr> 
                      @empty
                        <tr>
                          <td colspan="4" align="center">No Post</td>
                        </tr>
                      @endforelse
                   
                  </tbody>
                </table>
                <div class="mt-3">
                <a class="btn btn-primary" href="/films/create">Create New Post</a>
                </div>
              </div>
@endsection