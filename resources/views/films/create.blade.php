@extends('adminLTE.master') 

@section('content')


            <div class="card card-primary mt-3 ml-3 mr-3">    
            <div class="card-header">
                <h3 class="card-title">Create Films</h3>
            </div>
            <form role="form" action="/films" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="title">Title Films</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title','')}}" placeholder="Enter Title Films">
                    @error('title')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="body">Sinopsis Film</label>
                    <input type="text" class="form-control" id="body" name="body" value="{{old('body','')}}" placeholder="Enter Sinopsis">
                    @error('body')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
            </div>
           



@endsection