<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FilmController extends Controller
{
    public function create()
    {
        return view('films.create');
    } 

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required|unique:films|max:255',
            'body' => 'required'
        ]);

        $query = DB::table('films')->insert([
            'title' => $request["title"],
            'body' => $request["body"]
        ]);

        return redirect('/films')->with('success','Post Anda Telah Berhasil');
    }

    public function index(Request $request)
    {
        
        
        $films = DB::table('films')->get();
        // dd($films);
        return view('films.index', compact('films'));
    }

    Public function show($id)
    {
        $film = DB::table('films')->where('id', $id)->first();
        // dd($film);
        return view('films.show', compact('film'));
    }

    public function edit($id)
    {
        $film = DB::table('films')->where('id', $id)->first();

        return view('films.edit', compact('film'));
    }

    public function update($id, Request $request)
    {

        $request->validate([
            'title' => 'required|unique:films|max:255',
            'body' => 'required'
        ]);

        $film = DB::table('films')
                ->where('id', $id)
                ->update([
                    'title' => $request['title'],
                    'body' => $request['body']
                ]);

        return redirect('/films')->with('success','Post Anda Telah Berhasil Update');
    }

    public function destroy($id)
    {
        $film = DB::table('films')->where('id', $id)->delete();
        return redirect('/films')->with('success', 'Berhasil Menghapus');
    }
}
